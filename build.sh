#!/bin/sh
dotnet restore -r ubuntu.16.04-x64
dotnet publish -c Release -r ubuntu.16.04-x64
mkdir -p AppDir/usr/bin
cp bin/Release/netcoreapp3.1/ubuntu.16.04-x64/publish/* AppDir/usr/bin/
wget -c "https://github.com/AppImage/AppImageKit/releases/download/continuous/appimagetool-x86_64.AppImage"
chmod a+x appimagetool-x86_64.AppImage
./appimagetool-x86_64.AppImage ./AppDir
