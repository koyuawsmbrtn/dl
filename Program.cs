﻿using System;
using System.IO;
using System.Net;

namespace dl
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string url = "";
                if (args[0] == "-n")
                {
                    url = args[1];
                }
                else
                {
                    url = args[0];
                }
                string filename = url.Split('/')[url.Split('/').Length - 1];
                if (args[0] == "-n")
                {
                    if (!File.Exists(filename))
                    {
                        download(url, filename);
                    } else
                    {
                        Console.WriteLine("File " + filename + " already exists.");
                    }
                }
                else
                {
                    download(url, filename);
                }
            } catch
            {
                Console.WriteLine("Error downloading file. Please check your internet connection or use some of the options below.");
                Console.WriteLine("- Substitute a URL");
                Console.WriteLine("- Use -n to check if file exists");
            }
        }

        static void download(string url, string filename)
        {
            Console.WriteLine("Downloading " + url + "...");
            new WebClient().DownloadFile(url, filename);
            Console.WriteLine("File " + filename + " downloaded.");
        }
    }
}
